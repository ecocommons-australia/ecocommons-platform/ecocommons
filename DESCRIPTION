Package: ecocommons
Title: Functions for the EcoCommons platform
Version: 1.2.13
Authors@R: 
    person(given = "EcoCommons",
           family = "Platform",
           role = c("aut", "cre"),
           email = "contact@ecocommons.org.au",
           comment = c(ORCID = "YOUR-ORCID-ID"))
Description: This package aims to provide a series of useful functions to run
             species distribution models and other spatial analyses on the 
             EcoCommons platform
URL: https://ecocommons-australia.gitlab.io/ecocommons-platform/ecocommons/, https://gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons
Depends:
   R (>= 4.0.0)
Imports: 
  biomod2 (>= 4.1-2),
  boot,
  caret,
  dismo,
  gbm,
  ggdendro,
  geojsonsf,
  ggplot2,
  gridExtra,
  gstat,
  here,
  lme4,
  MASS,
  mgcv,
  ordinal,
  pROC,
  raster,
  remotes,
  reshape2,
  rgeos,
  rgdal,
  rjson,
  rmaxent,
  rpart,
  rpart.plot,
  sf,
  sp,
  terra,
  tidyr,
  tidyterra,
  viridis,
  visreg,
  xfun
Remotes:
  johnbaums/rmaxent,
  atlasoflivingaustralia/potions
License: MIT + file LICENSE
Encoding: UTF-8
LazyData: true
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.2.1
Suggests: 
    testthat (>= 3.0.0),
    BH,
    httpgd,
    intervals,
    unigd,
    xts,
    languageserver,
    rmarkdown
Config/testthat/edition: 3
