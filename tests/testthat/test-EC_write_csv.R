# test EC_write_csv

test_that("get error message if filename not provided", {
  expect_error(EC_write_csv(mtcars))
})


test_that("eval_tables/ directory gets written", {
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_circles.json"))
  json_file <- reset_json_paths(source_file)
  EC.env <- json_file$env
  out <- EC.env$outputdir
  dir.create(file.path2(out, "eval_tables"), showWarnings = FALSE)

  expect_true(paste0(out, "/eval_tables") %in% list.dirs(out))
})
