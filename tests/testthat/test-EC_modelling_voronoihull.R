test_that("EC_modelling_voronoihull outputs correctly for continuous data", {
  clean_outputs()

  # load everything for running EC_modelling_voronoihull()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_voronoihull.json"))
  json_file <- reset_json_paths(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  EC_modelling_voronoihull(EC.params, EC.env, response, predictor, dataset, model_compute)

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+voronoihull.+rds")), 1)

  # 1 png and 1 tif file in eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "_voronoihull.png$")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "_voronoihull.tif$")), 1)

  # test for files in eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)


  clean_outputs()
})

test_that("EC_modelling_voronoihull outputs correctly for categorical data", {
  clean_outputs()

  # load everything for running EC_modelling_voronoihull()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_voronoihull.json"))
  json_file <- reset_json_paths_cat(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )

  # can't run with categorical data only
  expect_error(EC_modelling_voronoihull(EC.params, EC.env, response, predictor, dataset, model_compute))

  clean_outputs()
})
