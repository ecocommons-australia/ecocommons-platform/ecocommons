test_that("json files can be imported", {
  json_file <- EC_read_json(
    test_path("input_files", "test_constraint_map_ann.json")
  )
  expect_gt(length(json_file), 1)
})

test_that("json files have the correct slot names", {
  source_file <- EC_read_json(here::here(
    "tests",
    "testthat",
    "input_files",
    "test_constraint_map_circles.json"
  ))

  expect_equal(names(source_file), c("user", "params", "env"))
})
