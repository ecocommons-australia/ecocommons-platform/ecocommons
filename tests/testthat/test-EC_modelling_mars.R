test_that("EC_modelling_mars outputs correctly for continuous data", {
  clean_outputs()

  # load everything for running EC_modelling_mars()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_mars.json"))
  json_file <- reset_json_paths(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_mars(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+MARS.+rds")), 1)

  # expect 7 "Full" outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Full-error-rates+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-functions+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-intervals+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-hist+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-plot+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-ROC+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-TPR-TNR+.+MARS.")), 1)

  # expect 4 Proj_current in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+MARS_unconstrained.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+MARS_unconstrained.tif$")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.tif$")), 1)

  # expect 1 VIP plot
  expect_equal(length(list.files(path = "./eval", pattern = "VIP_plot+.+MARS.png")), 1)

  # test for files in eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_MARS.txt")), 1)

  clean_outputs()
})

test_that("EC_modelling_mars outputs correctly for categorical data", {
  clean_outputs()

  # load everything for running EC_modelling_mars()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_mars.json"))
  json_file <- reset_json_paths_cat(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_mars(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+MARS.+rds")), 1)

  # expect 7 "Full" outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Full-error-rates+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-functions+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-intervals+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-hist+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-plot+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-ROC+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-TPR-TNR+.+MARS.")), 1)

  # expect 2 Proj_current in /eval folder (unconstrained models are not generated for categorical data)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.tif$")), 1)

  # expect 1 VIP plot
  expect_equal(length(list.files(path = "./eval", pattern = "VIP_plot+.+MARS.png")), 1)

  # test for files in eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_MARS.txt")), 1)

  clean_outputs()
})

test_that("EC_modelling_mars outputs correctly  with one categorical and one
 continuous layer", {
  clean_outputs()

  # load everything for running EC_modelling_mars()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_mars.json"))
  json_file <- reset_json_paths_mixed(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_mars(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+MARS.+rds")), 1)

  # expect 7 "Full" outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Full-error-rates+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-functions+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-intervals+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-hist+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-plot+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-ROC+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-TPR-TNR+.+MARS.")), 1)

  # expect 2 Proj_current in /eval folder (unconstrained models are not generated for categorical data)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.tif$")), 1)

  # expect 1 VIP plot
  expect_equal(length(list.files(path = "./eval", pattern = "VIP_plot+.+MARS.png")), 1)

  # test for files in eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_MARS.txt")), 1)

  clean_outputs()
})

test_that("EC_modelling_mars outputs correctly for continuous data and split", {
  clean_outputs()

  # load everything for running EC_modelling_mars()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_mars.json"))
  json_file <- reset_json_paths(source_file)
  EC.params <- json_file$params
  EC.params$data_split <- 80
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_mars(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+MARS.+rds")), 1)

  # expect 7 "Full" outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Full-error-rates+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-functions+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-loss-intervals+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-hist+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-presence-absence-plot+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-ROC+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Full-TPR-TNR+.+MARS.")), 1)

  # expect 4 Proj_current in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+MARS_unconstrained.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+MARS_unconstrained.tif$")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+MARS.tif$")), 1)

  # expect 1 VIP plot
  expect_equal(length(list.files(path = "./eval", pattern = "VIP_plot+.+MARS.png")), 1)

  # test for files in eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+MARS.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_MARS.txt")), 1)

  clean_outputs()
})
