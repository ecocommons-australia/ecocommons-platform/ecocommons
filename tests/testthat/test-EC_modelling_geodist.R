test_that("EC_modelling_geodist outputs correctly for continuous data", {
  clean_outputs()

  # load everything for running EC_modelling_geodist()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_geodist.json"))
  json_file <- reset_json_paths(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)

  EC_modelling_geodist(EC.params, EC.env, response, predictor, dataset)

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+geodist.+rds")), 1)

  # 1 png and 1 tif file in eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "_geodist.png$")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "_geodist.tif$")), 1)

  clean_outputs()
})

test_that("EC_modelling_geodist outputs correctly for categorical data", {
  # load everything for running EC_modelling_geodist()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_geodist.json"))
  json_file <- reset_json_paths_cat(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)

  # can't run with categorical data only
  expect_error(EC_modelling_geodist(EC.params, EC.env, response, predictor, dataset))

  clean_outputs()
})
