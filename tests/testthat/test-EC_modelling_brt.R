test_that("EC_modelling_brt outputs correctly for continuous data", {
  clean_outputs()

  # load everything for running EC_modelling_brt()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_brt.json"))
  json_file <- reset_json_paths(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_brt(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+BRT.+rds")), 1)

  # expect 7 dismo outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-error-rates+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-functions+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-intervals+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-hist+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-plot+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-ROC+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-TPR-TNR+.+BRT.")), 1)

  # expect 4 Proj_current in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT_unconstrained.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT_unconstrained.tif$")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+BRT.tif$")), 1)

  # expect 9 outputs  in /eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Biomod2_like_VariableImportance+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Maxent_like_VariableImportance+.+BRT.")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_BRT.txt")), 1)

  clean_outputs()
})

# TODO: I think SRE strategy has been deprecated so no need to test for this.
# Just need to confirm that this is the case before removing.
test_that("EC_modelling_brt outputs correctly for continuous data and absences calculated with SRE", {
  clean_outputs()

  # load everything for running EC_modelling_brt()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_brt.json"))
  json_file <- reset_json_paths(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "sre", # TODO change this
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_brt(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+BRT.+rds")), 1)

  # expect 7 dismo outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-error-rates+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-functions+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-intervals+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-hist+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-plot+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-ROC+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-TPR-TNR+.+BRT.")), 1)

  # expect 4 Proj_current in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT_unconstrained.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT_unconstrained.tif$")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+BRT.tif$")), 1)

  # expect 9 outputs  in /eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Biomod2_like_VariableImportance+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Maxent_like_VariableImportance+.+BRT.")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_BRT.txt")), 1)

  clean_outputs()
})



# _________________________________________________________
test_that("EC_modelling_brt outputs correctly for categorical data", {
  # load everything for running EC_modelling_brt()
  clean_outputs()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_brt.json"))
  json_file <- reset_json_paths_cat(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_brt(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+BRT.+rds")), 1)

  # expect 7 dismo outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-error-rates+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-functions+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-intervals+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-hist+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-plot+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-ROC+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-TPR-TNR+.+BRT.")), 1)

  # expect expect 2 Proj_current in /eval folder (unconstrained models are not generated for categorical data)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+BRT.tif$")), 1)

  # expect 9 outputs  in /eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Biomod2_like_VariableImportance+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Maxent_like_VariableImportance+.+BRT.")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_BRT.txt")), 1)

  clean_outputs()
})

test_that("EC_modelling_brt outputs correctly with one categorical and one
continuous predictor", {
  # load everything for running EC_modelling_brt()
  clean_outputs()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_brt.json"))
  json_file <- reset_json_paths_mixed(source_file)
  EC.params <- json_file$params
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_brt(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+BRT.+rds")), 1)

  # expect 7 dismo outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-error-rates+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-functions+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-intervals+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-hist+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-plot+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-ROC+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-TPR-TNR+.+BRT.")), 1)

  # expect expect 2 Proj_current in /eval folder (unconstrained models are not generated for categorical data)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+BRT.tif$")), 1)

  # expect 9 outputs  in /eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Biomod2_like_VariableImportance+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Maxent_like_VariableImportance+.+BRT.")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_BRT.txt")), 1)

  clean_outputs()
})

test_that("EC_modelling_brt outputs correctly for continuous data and
a data split", {
  clean_outputs()

  # load everything for running EC_modelling_brt()
  source_file <- EC_read_json(here::here("tests", "testthat", "input_files", "test_constraint_map_brt.json"))
  json_file <- reset_json_paths(source_file)
  EC.params <- json_file$params
  EC.params$data_split <- 80
  EC.env <- json_file$env
  response <- EC_build_response(EC.params)
  predictor <- EC_build_predictor(EC.params)
  constraint <- EC_build_constraint(EC.params)
  dataset <- EC_build_dataset(EC.env, predictor, constraint, response)
  model_compute <- EC_format_biomod2(EC.env,
    true.absen               = dataset$absen,
    pseudo.absen.points      = dataset$pa_number_point,
    pseudo.absen.strategy    = "random",
    pseudo.absen.disk.min    = EC.params$pa_disk_min,
    pseudo.absen.disk.max    = EC.params$pa_disk_max,
    pseudo.absen.sre.quant   = EC.params$pa_sre_quant,
    climate.data             = dataset$current_climate,
    occur                    = dataset$occur,
    species.name             = response$occur_species,
    save.pseudo.absen        = TRUE,
    save.env.absen           = TRUE,
    save.env.occur           = TRUE,
    generate.background.data = FALSE
  )
  suppressWarnings(
    EC_modelling_brt(EC.params, EC.env, response, predictor, constraint, dataset, model_compute)
  )

  # rds object in /output
  expect_equal(length(list.files(path = ".", pattern = ".+BRT.+rds")), 1)

  # expect 7 dismo outputs in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-error-rates+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-functions+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-loss-intervals+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-hist+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-presence-absence-plot+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-ROC+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Dismo-TPR-TNR+.+BRT.")), 1)

  # expect 4 Proj_current in /eval folder
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT_unconstrained.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT_unconstrained.tif$")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current_+.+BRT.png")), 1)
  expect_equal(length(list.files(path = "./eval", pattern = "Proj_current+.+BRT.tif$")), 1)

  # expect 9 outputs  in /eval_tables folder
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Absence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Occurrence_environmental+")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Pseudo_absences+")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Biomod2_like_VariableImportance+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-data+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Evaluation-statistics+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Loss-function-intervals-table+.+BRT.")), 1)
  expect_equal(length(list.files(path = "./eval_tables", pattern = "Maxent_like_VariableImportance+.+BRT.")), 1)

  expect_equal(length(list.files(path = "./eval_tables", pattern = "Summary_BRT.txt")), 1)

  clean_outputs()
})
