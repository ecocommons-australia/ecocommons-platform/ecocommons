# test_EC_save_png

test_that("get error message if filename not provided", {
  expect_error(EC_save_png(mtcars))
})

test_that("get error message if output directory doesn't exist", {
  expect_error(EC_save_png(filename, paste0("eval/", filename, ".png")))
})

# test that it can save a plot in the temp directory, with 4 plot elements,
# ncol = 2, nrow = 2
test_that("Multiple plot objects can be passed to the function, arranged, and saved", {
  my_plot <- ggplot2::ggplot(
    data = cars,
    aes(x = speed, y = dist)
  ) +
    ggplot2::geom_point()

  EC_save_png(my_plot, my_plot, my_plot, my_plot,
    ncol = 2, nrow = 2,
    filename = "cars_are_fun", outputdir = here::here("tests", "testthat", "temp", "output")
  )

  expect_true(file.exists(
    here::here(
      "tests", "testthat",
      "temp", "output", "eval",
      "cars_are_fun.png"
    )
  ))
  clean_outputs()
})
