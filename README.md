<img src="man/figures/EcoCommons_logo.png" align="centre">

---

<p style="text-align: center;">
<a href="https://www.ecocommons.org.au/" target="_blank" style="display: inline-block;
background-color: #f9b072; color: #fff; padding: 10px 18px; border-radius: 5px;
font-size: 18px; text-decoration: none;">EcoCommons Platform Website</a>
<a href="https://ecocommons-australia.gitlab.io/ecocommons-platform/ecocommons/index.html" target="_blank" style="display: inline-block;
background-color: #1da589; color: #fff; padding: 10px 18px; border-radius: 5px;
font-size: 18px; text-decoration: none;">EcoCommons Package Website</a>
</p>
`ecocommons` is an R interface to run species distribution models
hosted by [EcoCommons](https://www.ecocommons.org.au/), a platform for ecological and environmental modelling and analysis.

This R package is optimised for the EcoCommons platform. As such, there are some aspects and functions of the package that may be unclear for users who wish to get started locally. However, we are actively working to improve our workflows and documentation for local use - whether you wish to conduct your own analysis, or repeat experiments that were run on the EcoCommons platform! As such, all feedback is welcome and appreciated!

Please visit our [GitLab Wiki](https://gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/-/wikis/home) to read more about the project and how you can get started. There you can also find the example workflows and [Vignettes](https://gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/-/wikis/bioclim-example) that we have available (stay tuned - more coming soon!)

If you have any comments, questions or suggestions, feel free to open an issue on GitLab, or get in touch with us:
contact@ecocommons.org.au

<br>

---

# Installation

To install the package, first install the `remotes` package:

```r
install.packages("remotes")
install.packages("git2r") # required for remotes::install_git to work
install_github('johnbaums/rmaxent') # also install this dependency which adds extra functions in Maxent
```

Then install the `ecocommons` package via `remotes` and load it:

```r
remotes::install_git(
  "https://gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons",
  upgrade = "never") 
library(ecocommons)
```

As the EcoCommons R package uses BIOMOD2, which is heavy in dependencies, the installation can take a while.

---

# Usage

Please use `inst/EcoCommons_source.R` as the main script to be followed for all
SDM algorithms, except for species traits modelling experiment (`EC_modelling_sptraits`).

If this is your first time using the package, we recommend checking out the [bioclim example workflow](https://gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/-/wikis/bioclim-example) in our Wiki, which uses some example data included with the package.  

When running each model, please note that each model requires a `.json` file to
be used as a `source_file`. Example `source_file` can be found on
`test/testthat/input_files`. Note that you should change for each new
algorithm, with the correct `source_file` for each model indicated by the suffix.

For example:  
`test_constraint_map_ann.json` should be used to run the ANN algorithm, whereas
`test_constraint_map_rf.json` should be used to run Random Forest (RF).

For species traits modelling experiment (`EC_modelling_sptraits`) and secondary
experiments that uses existing models as inputs, as Climate Change experiment and
Ensemble analysis (identified as `EC_projecting`), you don't require the script `inst/EcoCommons_source.R`. All you need is to load your example file and run
the interested function, as:

```r
source_file <- EC_read_json(file= here::here("tests", "testthat", "input_files", "test_ensemble.json"))

EC_projecting_ensemble(source_file)
```

---

# Pipelined builds [internal]

Docker images are built automatically via Gitlab pipeline.  
Package dependencies are seperated into a 'base' build (`Dockerfile-base`).
The base image will only be built on a manual pipeline run with the variable `BUILD_BASE = 1` set.

For detailed instructions on building Docker images for publishing see `.gitlab-ci`.

# Extra information

All the functions created within EcoCommons start with “EC_“. Each algorithm has
its own function, starting with “EC_modelling_“. Secondary experiments start with
"EC_projecting_".
Please note that subfunctions that are only used in one function are nested
above the main function.

