.onAttach <- function(.ecocommons, ecocommons) {
  string <-
    "
#########################################################################
#######            EcoCommons package: SDM algorithms             #######
#########################################################################
##
## Author details:   EcoCommons Platform
## Contact details:  comms@ecocommons.org.au
## Copyright phrase: This script is the product of the EcoCommons platform
##
##########################################################################
    "
  packageStartupMessage(
    string, " This is version: ",
    packageVersion(ecocommons),
    " of ", ecocommons
  )
}
