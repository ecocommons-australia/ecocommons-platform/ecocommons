#' Run an Boosted Regression Tree (BRT) within EcoCommons
#'
#' A BRT is a machine learning model that uses a combination of decision trees
#' and boosting to iteratively fit random subsets of data so that each new tree
#' takes the error of the last trees into account
#'
#' @param EC.params List created from a json file, containing source_file$params
#' @param EC.env List created from a json file, containing source_file$env with
#' save directories
#' @param response Response object; a nested named list created on
#' EC_build_response
#' @param predictor Predictor object; a nested named list created on
#' EC_build_predictor
#' @param constraint Constraint object; a nested named list created on
#' EC_build_constraint
#' @param dataset Dataset object; a nested named list created on
#' EC_build_dataset
#' @param model_compute Model for evaluation on 'biomod2' format ; created on
#' EC_format_biomod2
#'
#' @importFrom dismo gbm.step
#' @importFrom dismo predict
#'
#' @export EC_modelling_brt
EC_modelling_brt <- function(EC.params, # from source_file$EC.params
                             EC.env, # from source_file$EC.env
                             response, # from EC_build_response
                             predictor, # from EC_build_predictor
                             constraint, # from EC_build_constraint
                             dataset, # from EC_build_dataset
                             model_compute) {
  # Specify the algorithm running the experiment
  model_algorithm <- "BRT"

  # specific parameters to run brt algorithm
  model_options_brt <- EC_options_brt(
    EC.params,
    model_algorithm,
    response
  )

  # Extract occurrence and absence data
  coord <- cbind(model_compute@coord, model_compute@data.env.var)

  occur <- coord[
    c(which(model_compute@data.species == 1)),
    names(coord)
  ]

  absen <- coord[
    c(
      which(model_compute@data.species == 0 |
        is.na(model_compute@data.species))
    ),
    names(coord)
  ]

  # set up brt.data
  brt.data <- coord
  brt.data$pa <- c(rep(1, nrow(occur)), rep(0, nrow(absen)))

  setwd(EC.env$outputdir)

  model_sdm <- dismo::gbm.step(
    data = brt.data,
    gbm.x = which(names(brt.data) %in%
      names(dataset$current_climate)),
    gbm.y = which(names(brt.data) == "pa"),
    tree.complexity = model_options_brt$brt.tree.complexity,
    learning.rate = model_options_brt$brt.learning.rate,
    bag.fraction = model_options_brt$brt.bag.fraction,
    site.weights = model_options_brt$brt.site.weights,
    var.monotone = model_options_brt$brt.var.monotone,
    n.folds = model_options_brt$brt.n.folds,
    prev.stratify = model_options_brt$brt.prev.stratify,
    family = model_options_brt$brt.family,
    n.trees = model_options_brt$brt.n.trees,
    step.size = model_options_brt$brt.step.size,
    max.trees = model_options_brt$brt.max.trees,
    tolerance.method = model_options_brt$brt.tolerance.method,
    tolerance = model_options_brt$brt.tolerance,
    keep.data = model_options_brt$brt.keep.data,
    plot.main = model_options_brt$brt.plot.main,
    plot.folds = model_options_brt$brt.plot.folds,
    verbose = model_options_brt$brt.verbose,
    silent = model_options_brt$brt.silent,
    keep.fold.models = model_options_brt$brt.keep.fold.models,
    keep.fold.vector = model_options_brt$brt.keep.fold.vector,
    keep.fold.fit = model_options_brt$brt.keep.fold.fit
  )

  # Save a summary document
  dir.create(file.path2(EC.env$outputdir, "eval_tables/"),
    showWarnings = FALSE
  )
  sink(
    paste0(
      EC.env$outputdir,
      "/eval_tables/Summary_BRT.txt"
    ),
    type = "output"
  )
  print(summary(model_sdm))
  sink()

  # Save the model object
  EC_save(model_sdm,
    name = paste0(
      model_options_brt$species_algo_str,
      "_model_object.rds"
    ),
    EC.env$outputdir
  )

  # 1. Prediction without constraint if all env data layers are continuous
  if (constraint$genUnconstraintMap &&
    all(predictor$type == "continuous") &&
    (!is.null(constraint$constraints) || constraint$generateCHull)) {
    model_proj <- dismo::predict(dataset$current_climate_orig,
      model_sdm,
      n.trees = model_sdm$gbm.call$best.trees,
      type = "response",
      na.rm = TRUE, filename = "proj_full.tif", overwrite = TRUE
    )

    # Save the projection in png and tif files
    projection_strings <- EC_save_projection_prep_dismo(
      EC.env,
      model_algorithm, model_options_brt$species_algo_str, "unconstrained"
    )

    EC_save_projection_generic(
      model_proj,
      projection_strings$filename_gg,
      projection_strings$filename_tif,
      projection_strings$plot_title
    )
  }

  # 2. Prediction with constraint
  model_proj <- dismo::predict(dataset$current_climate,
    model_sdm,
    n.trees = model_sdm$gbm.call$best.trees,
    type = "response",
    na.rm = TRUE, filename = "proj_const.tif", overwrite = TRUE
  )

  # Save the prediction in png and tif files
  projection_strings <- EC_save_projection_prep_dismo(
    EC.env,
    model_algorithm, model_options_brt$species_algo_str
  )

  EC_save_projection_generic(
    model_proj,
    projection_strings$filename_gg,
    projection_strings$filename_tif,
    projection_strings$plot_title
  )

  # Evaluate model
  EC_save_dismo_eval(
    EC.env,
    model_algorithm,
    model_sdm,
    occur,
    absen,
    model_options_brt$species_algo_str
  )
  # Remove temp raster files
  terra::tmpFiles(remove = TRUE)
  raster::removeTmpFiles()

  message("Completed species distribution modelling with algorithm 'BRT'\n")
}

#' Internal function to create a list of options for EC_modelling_brt
#'
#' @param a List containing the EC.params
#' @param model_algorithm String containing the name of the algorithm
#' @param response List from EC_build_response
#'
#' @return List of options for BRT
EC_options_brt <- function(a,
                           model_algorithm,
                           response) {
  list(
    # a fold vector to be read in for cross validation with offsets
    brt.fold.vector = NULL,
    # sets the complexity of individual trees
    brt.tree.complexity = a$tree_complexity,
    # sets the weight applied to individual trees
    brt.learning.rate = a$learning_rate,
    # sets the proportion of observations used in selecting variables
    brt.bag.fraction = a$bag_fraction,
    # rep(1, nrow(data)) #allows varying weighting for sites
    brt.site.weights = NULL,
    # rep(0, length(gbm.x)) #restricts responses to individual predictors to
    # monotone
    brt.var.monotone = NULL,
    # number of folds
    brt.n.folds = a$n_folds,
    # prevalence stratify the folds - only for presence/absence data
    brt.prev.stratify = a$prev_stratify,
    # family - bernoulli (=binomial), poisson, laplace or gaussian
    brt.family = a$family,
    # number of initial trees to fit
    brt.n.trees = a$n_trees,
    # numbers of trees to add at each cycle
    brt.step.size = a$n_trees,
    # max number of trees to fit before stopping
    brt.max.trees = a$max_trees,
    # method to use in deciding to stop - "fixed" or "auto"
    brt.tolerance.method = a$tolerance_method,
    # tolerance value to use - if method == fixed is absolute, if auto is
    # multiplier * total mean deviance
    brt.tolerance = a$tolerance_value,
    # Logical. keep raw data in final model
    brt.keep.data = FALSE,
    # Logical. plot hold-out deviance curve
    brt.plot.main = FALSE,
    # Logical. plot the individual folds as well
    brt.plot.folds = FALSE,
    # Logical. control amount of screen reporting
    brt.verbose = FALSE,
    # Logical. to allow running with no output for simplifying model)
    brt.silent = FALSE,
    # Logical. keep the fold models from cross validation
    brt.keep.fold.models = FALSE,
    # Logical. allows the vector defining fold membership to be kept
    brt.keep.fold.vector = TRUE,
    # Logical. allows the predicted values for observations from
    # cross-validation to be kept
    brt.keep.fold.fit = FALSE,
    projection_name = "current",
    species_algo_str = ifelse(is.null(a$subset),
      paste0(
        response$occur_species, "_",
        model_algorithm
      ),
      paste0(
        response$occur_species, "_",
        model_algorithm, a$subset
      )
    )
  )
}
