#' Run a Climate Change (CC) projection within EcoCommons
#'
#' * A CC projection predicts the distribution of a species under potential
#'   future climatic conditions.
#' * It uses the output model from a previously run species distribution model
#'   (using EcoCommons) to predict a distribution based on potential
#'   future climate datasets and climate change scenarios.
#' * This function primarily prepares the input data for the CC projection, and
#'   then calls `EC_cc_in_proj()` to generate the predictions.
#'
#' @param climate_change_file List; a nested named list of the metadata and job
#' parameters required for running `EC_projecting_CC()`, namely:
#' \describe{
#'  \item{params}{List; a nested named list of the job parameters, including the
#'  species, environmental data, and model parameters.}
#'  \item{env}{List; a named list of the different directories.}
#' }
#'
#' @importFrom xfun file_ext
#'
#' @export EC_projecting_cc
EC_projecting_cc <- function(climate_change_file) {
  EC.params <- climate_change_file$params

  ## 02/28/2023 output and workdir are same, temp/output
  ## This is intentional
  EC.env <- climate_change_file$env

  # Before starting, set the seed
  EC_set_seed(EC.params$random_seed)

  # Set working directory (script runner takes care of it)

  setwd(EC.env$workdir)

  # Extract nested parameters into objects for readability
  sdm_species <- EC.params$species_distribution_models$species
  sdm_model_file <- EC.params$species_distribution_models$filename
  sdm_algorithm <- EC.params$sdm_function
  sdm_projections_files <- lapply(EC.params$sdm_projections, function(x) {
    x$filename
  })

  ## 02/28/2023 New projection dataset name
  projection_name <- EC.params$projection_name

  # Get the prediction threshold (or set to 0.5 if null)
  projection_threshold <- ifelse(is.null(EC.params$threshold),
    0.5,
    EC.params$threshold
  )

  # Set region offset
  if (is.null(EC.params$modelling_region$radius) ||
    is.na(EC.params$modelling_region$radius) ||
    EC.params$modelling_region$radius == "") {
    EC.params$modelling_region$radius <- 0
    region_offset <- EC.params$modelling_region$radius
  } else {
    region_offset <- as.double(EC.params$modelling_region$radius)
    region_offset <- ifelse(!is.na(EC.params$modelling_region$radius) &&
      is.numeric(EC.params$modelling_region$radius),
    EC.params$modelling_region$radius / 111.0, 0
    ) # convert from km to degree
  }

  # TODO: consistency in terminology? scale down vs finest coarsest vs highest
  # lowest etc. A little confusing / inconsistent throughout package
  # If scale down is false, the resampling flag is "lowest"
  # If scale down is true, the resampling flag is "highest"
  # 02/28/2023 NOTE: there is no input on front end for resampling. Is this
  # taken from the original model params? If I select a 5km future dataset and
  # my source is 1km, what happens? Should this be a user option?
  resamplingflag <- if (!is.null(EC.params$base_layer)) {
    "base_layer"
  } else {
    ifelse(is.null(EC.params$scale_down) ||
             EC.params$scale_down == "finest_resolution" ||
             # original Boolean option for maintaining backward compatibility
             EC.params$scale_down == TRUE,
           "highest", "lowest"
    )
  }

  # Extract nested parameters into objects
  future_dataset <- lapply(
    EC.params$future_climate_datasets,
    function(x) x$filename
  )
  future_type <- lapply(
    EC.params$future_climate_datasets,
    function(x) x$type
  )
  future_layer <- lapply(
    EC.params$future_climate_datasets,
    function(x) x$layer
  )

  # Layer names for the selected future dataset layers
  # TODO: this is the same outcome as future_layer, just as a vector instead of
  # list. Probably unncessary.
  future_selected_layers <- EC.params$selected_future_layers

  ## 02/28/2023 Unsure what subset is for / does, multiple species?
  mm_subset <- EC.params$subset

  sdm_species <- gsub(" ", "_", sdm_species)

  # Todo - is having spaces in the species name a problem? Probably
  species_algo_str <- ifelse(is.null(mm_subset),
    paste0(sdm_species, "_", sdm_algorithm),
    paste0(sdm_species, "_", sdm_algorithm, mm_subset)
  )

  # if SDM model is a zip file we'll have to unzip it
  # Unzip the model into working directory
  if (tolower(xfun::file_ext(sdm_model_file)) == "zip") {
    unzip(sdm_model_file, exdir = EC.env$workdir)
    sdm_model_file <- list.files(pattern = "*.rds$")[[1]]
    model_obj <- readRDS(paste0(EC.env$workdir, "/", sdm_model_file))
  } else {
    model_obj <- readRDS(sdm_model_file)
  }

  # if there is a problem with biomod2 models, correct them
  model_obj <- decorrupt_biomod_model(model_obj)

  # list the layers of interest for the future projections
  selected_layer_indexes <- lapply(future_selected_layers, match, future_layer)

  # 02/28/2023 added: just filter out unused layers from future_scenario here so
  # only selected layers are passed through EC_raster_stack, and therefore
  # selected_layers = NULL
  future_dataset <- future_dataset[unlist(selected_layer_indexes)]

  # adjust future raster layers to same projection, resolution and extent
  future_scenario <- EC_raster_stack(future_dataset,
    future_type,
    future_layer,
    resamplingflag  = resamplingflag,
    selected_layers = NULL
  )

  # TODO where does the model region come into play???

  # Returns a raster stack
  predictors <- EC_check_model_layers(
    model_obj,
    future_scenario,
    future_dataset
  )

  # Use folder name of first dataset to generate name for projection output
  EC_cc_in_proj(
    EC.params,
    EC.env,
    sdm_algorithm,
    species_algo_str,
    model_obj,
    predictors,
    resamplingflag,
    projection_name,
    projection_threshold,
    sdm_species,
    region_offset,
    sdm_projections_files
  )

  # Remove temp raster files
  terra::tmpFiles(remove = TRUE)
  raster::removeTmpFiles()

  return(message("Climate Change (CC) projection completed!\n"))
}
