#' Function to to test  how traits differ among species (requires multiple input
#' species).
#'
#'
#' @param traitdiff_file read the json species trait difference file
#'
#' @importFrom MASS polr
#' @importFrom mgcv multinom
#' @importFrom terra crs
#'
#' @export EC_modelling_sptraits_traitdiff

############################################################
###       EcoCommons script to run SDM algorithms        ###
###       using species traits difference data           ###
############################################################
##
## Author details:   EcoCommons Platform
## Contact details:  comms@ecocommons.org.au
## Date:             March 2022
## Copyright phrase: This script is the product of EcoCommons platform.
##
## In this script you will:
## - Read input file
##
## - Set up parameters that will allow you to run a Generalized Linear Model (GLM)
##    using species traits data to test how traits differ among species
##
## - Run the select algorithm
##

# sptraits_file <- EC_read_json(file="~/Documents/GitHub/ecocommons/tests/testthat/input_files/test_sptraits_traitdiff.json")

EC_modelling_sptraits_traitdiff <- function(sptraits_file) {
  EC.params <- sptraits_file$params # data file with species, environment data and parameters
  EC.env <- sptraits_file$env # set workplace environment

  # before start, set seed
  EC_set_seed(EC.params$random_seed)

  # 2. Set working directory (script runner takes care of it)
  setwd(EC.env$workdir)

  # read files
  trait_filename <- EC.params$traits_dataset$filename # input dataset csv file
  trait_params <- EC.params$traits_dataset_params # names of input dataset
  trait_data <- read.csv(trait_filename) # trait data

  constraints <- if (!is.null(EC.params$modelling_region$filename)) { # geographic constraints
    readLines(EC.params$modelling_region$filename, warn = FALSE)
  } else {
    NULL
  }

  na_action <- if (!is.null(EC.params$na_action)) {
    EC.params$na_action
  } else {
    get("na.fail")
  }

  # Empty environmental dataset as it is not needed
  crs(current_climate) <- "+init=epsg:4326"

  # Geographically constrained modelling; just need to constraint the trait-data
  if (!is.null(trait_data)) {
    merged_result <- EC_sptrait_constraint(EC.env,
      EC.params,
      trait_data,
      trait_params,
      current_climate,
      constraints,
      region_offset = FALSE,
      generateCHull = FALSE
    )
    trait_data <- merged_result$data
    trait_params <- merged_result$params
  }

  # Generate the formulae to test differences among species for each trait separately
  # trait ~ species
  formulae <- EC_sptrait_formulae(EC.params,
    trait_params,
    trait_diff = TRUE
  )

  # Run model - with polr function for ordinal traits, multinom function for nominal traits,
  #             glm function for continuous traits

  for (formula in formulae) {
    trait_name <- gsub("[_ ]", "-", trimws(formula$trait))
    if (formula$type == "ordinal") {
      output_filename <- paste0(trait_name, "_diffPOLR_results.txt")
      glm_result <- MASS::polr(
        formula = as.formula(formula$formula),
        data = trait_data,
        weights = NULL,
        na.action = na_action,
        contrasts = NULL,
        Hess = TRUE,
        model = TRUE,
        method = "logistic"
      )
    } else if (formula$type == "nominal") {
      output_filename <- paste0(trait_name, "_diffNOM_results.txt")
      glm_result <- mgcv::multinom(
        formula = as.formula(formula$formula),
        data = trait_data,
        weights = NULL,
        na.action = na_action,
        contrasts = NULL,
        summ = 0,
        model = TRUE
      )
    } else {
      output_filename <- paste0(trait_name, "_diffGLM_results.txt")
      glm_result <- glm(
        formula = as.formula(formula$formula),
        family = EC_family_string(EC.params$family),
        data = trait_data,
        weights = NULL,
        na.action = na_action,
        start = NULL,
        etastart = NULL,
        mustart = NULL,
        offset = NULL,
        model = TRUE,
        method = EC.params$method,
        x = FALSE,
        y = FALSE,
        contrasts = NULL
      )
    }

    ## Save the result to file
    # Save the model
    EC_save(glm_result, name = paste0(trait_name, "_traitdiff_GLM_model.object.rds"), EC.env$outputdir)

    ## Save the results as text to file for each trait
    summary_traitdiff <- summary(glm_result)
    dir.create(file.path2(EC.env$outputdir, "eval_tables/"), showWarnings = FALSE)
    file <- file.path2(EC.env$outputdir, "eval_tables/", paste0(output_filename))
    capture.output(summary_traitdiff, file = file)
  }

  # Remove temp raster files
  # terra::tmpFiles(remove = TRUE)
  # raster::removeTmpFiles()
}
