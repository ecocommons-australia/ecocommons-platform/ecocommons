#' Save CSV Data in `outputdir`
#'
#' @param robj R object to be saved
#' @param name name of the file
#' @param outputdir path to output directory (EC.env$outputdir)
#' @param rownames logical indicating whether to save row names
#'
#' @export EC_write_csv
EC_write_csv <- function(robj,
                         name,
                         outputdir = EC.env$outputdir,
                         rownames = TRUE) {
  dir.create(file.path2(outputdir, "eval_tables/"), showWarnings = FALSE)

  filename <- file.path2(outputdir, "eval_tables/", name)

  write.csv(robj,
    file = filename,
    row.names = rownames
  )
}
