#' Run an Inverse Distance Weighted Model within EcoCommons
#'
#' An Inverse Distance Weighted Model is a geographical model to predict species
#' probabilities in unknown locations by averaging values of nearby occurrences.
#' It uses the `biomod2` package
#' to set up parameters, `dismo` functions, and the input data generated on the
#' EcoCommons platform.
#'
#' @param EC.params List created from a json file, containing source_file$params
#' @param EC.env List created from a json file, containing source_file$env with
#' save directories.
#' @param response Response object; a nested named list created on
#' EC_build_response.
#' @param predictor Predictor object; a nested named list created on
#' EC_build_predictor.
#' @param dataset Dataset object; a nested named list created on
#' EC_build_dataset.
#' @param model_compute Model for evaluation on 'biomod2' format ; created on
#' EC_format_biomod2.
#'
#' @importFrom dismo geoIDW
#' @importFrom dismo predict
#' @importFrom raster raster
#'
#' @export EC_modelling_geoidw
EC_modelling_geoidw <- function(EC.params,
                                EC.env,
                                response,
                                predictor,
                                dataset,
                                model_compute) {
  if (!all(predictor$type == "continuous")) {
    stop("Inverse Distance Weighted Model function not run because categorical
    data cannot be used\n")
  }
  # Check if current_climate is in `RasterLayer` format
  if (length(dataset$current_climate) == 1) {
    if (!inherits(
      dataset$current_climate,
      c("RasterLayer", "RasterStack", "RasterBrick")
    )) {
      current_climate <- EC_dismo_convert_stack(dataset$current_climate)
      names(current_climate) <- names(model_compute@data.env.var)
    } else {
      current_climate <- dataset$current_climate
    }
  }

  # Set parameters to perform modelling
  model_algorithm <- "geoIDW"
  species_algo_str <- paste0(response$occur_species, "_", model_algorithm)

  # Parameters to perform modelling
  coord <- model_compute@coord
  occur <- coord[c(which(model_compute@data.species == 1)), names(coord)]
  absen <- coord[
    c(which(model_compute@data.species == 0 |
      is.na(model_compute@data.species))),
    names(coord)
  ]

  setwd(EC.env$outputdir)

  model_sdm <- dismo::geoIDW(occur, absen)

  # Save out the model object
  EC_save(model_sdm,
    name = paste0(species_algo_str, "_model_object.rds"), EC.env$outputdir
  )

  model_proj <- dismo::predict(model_sdm,
    current_climate[[1]],
    mask = TRUE,
    filename = "proj_const.tif",
    overwrite = TRUE
  )

  # Save the projection in png and tif files
  projection_strings <- EC_save_projection_prep_dismo(
    EC.env,
    model_algorithm,
    species_algo_str
  )

  EC_save_projection_dismo(
    model_proj,
    projection_strings$filename_gg,
    projection_strings$filename_tif,
    projection_strings$plot_title
  )

  # Remove temp raster files
  terra::tmpFiles(remove = TRUE)
  raster::removeTmpFiles()

  return(message("Inverse Distance Weighted Model completed!\n"))
}
