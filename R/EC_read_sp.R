#' Read species presence/absence data
#
#' To run Species Distribution Models (SDMs) we first need to set up the
#' necessary dataset. This script aims to check if the species geographic
#' data is on the right format to be read. Latitude and longitude
#' should be numeric, and month and year information should be kept
#'
#' @param filename string with the path to the species data
#' @param crs EPSG string for CRS (default is EPSG:4326)
#' @param month_filter list(?) with the months to be kept
#' @param species_filter list(?) with the species to be kept
#'
#' @export EC_read_sp
EC_read_sp <- function(filename, # from response_info
                       crs = NULL, # "EPSG:4326"
                       base_crs = "4326", # base layer crs
                       month_filter = NULL,
                       species_filter = NULL) {
  if (!is.null(filename)) { # return NULL if filename is not given
    
    csvfile <- read.csv(filename)
    
    # Rename x and y to lon and lat if needed
    names(csvfile)[names(csvfile) == "x"] <- "lon"
    names(csvfile)[names(csvfile) == "y"] <- "lat"
    
    # Convert to crs of base layer if required
    if (!is.null(crs) && terra::crs(crs, describe = TRUE)$code != base_crs) {
      cat(
        "Reprojecting spatial points from",
        crs, "to epsg:", base_crs, "\n"
      )
      csvfile[,c("lon", "lat")] <- terra::as.data.frame(
        terra::project(terra::vect(csvfile, crs = crs), 
          paste0("EPSG:", base_crs)), 
        geom = "XY")[,c("x", "y")]
    }

    # keep only lon and lat columns; for MM include month column
    # what is MM? Migratory modelling?
    col_kept <- c("lon", "lat")

    if (!is.null(month_filter)) {
      col_kept <- append(col_kept, "month")
      csvfile <- subset(csvfile, month %in% unlist(month_filter))
    }

    if (!is.null(species_filter) & "species" %in% colnames(csvfile)) {
      col_kept <- append(col_kept, "species")
      csvfile <- subset(csvfile, species %in% unlist(species_filter))
    }
    
    # Clean any missing lon/lat values
    invalid_idx <- unique(which(is.na(
      as.matrix(csvfile[,c("lon", "lat"), drop = FALSE])), arr.ind = TRUE)[,1])
    if (length(invalid_idx > 0)) {
      csvfile <- csvfile[-invalid_idx,]
    }
    
    return(csvfile[col_kept])
  }
}
