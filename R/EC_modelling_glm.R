#' Run a Generalized Linear Model (GLM) within EcoCommons
#'
#' A GLM is a statistical regression, fitted with maximum likelihood estimation
#' for data with non-normal distribution
#'
#' @param EC.params List created from a json file, containing source_file$params
#' @param EC.env List created from a json file, containing source_file$env with
#' save directories
#' @param response Response object; a nested named list created on
#' EC_build_response
#' @param predictor Predictor object; a nested named list created on
#' EC_build_predictor
#' @param constraint Constraint object; a nested named list created on
#' EC_build_constraint
#' @param dataset Dataset object; a nested named list created on
#' EC_build_dataset
#' @param model_compute Model for evaluation on 'biomod2' format ; created on
#' EC_format_biomod2
#'
#' @importFrom biomod2 BIOMOD_ModelingOptions
#' @importFrom biomod2 BIOMOD_Modeling
#' @importFrom biomod2 bm_VariablesImportance
#' @importFrom biomod2 get_formal_model
#' @importFrom boot glm.diag.plots
#' @importFrom reshape2 melt
#'
#' @export EC_modelling_glm

EC_modelling_glm <- function(EC.params, # from source_file$EC.params
                             EC.env, # from source_file$EC.env
                             response, # from EC_build_response
                             predictor, # from EC_build_predictor
                             constraint, # from EC_build_constraint
                             dataset, # from EC_build_dataset
                             model_compute) { # from EC_format_biomod2

  # Set parameters to perform modelling

  # Specify the algorithm running the experiment
  model_algorithm <- "GLM"

  # specific parameters to run GLM algorithm
  model_options_glm <- EC_options_glm(EC.params)

  # general parameters to run biomod2 package modelling
  model_options_algorithm <- EC_options_algorithm(
    EC.params,
    response,
    model_algorithm
  )
  # Define the model options
  model_options <- biomod2::BIOMOD_ModelingOptions(GLM = model_options_glm)

  setwd(EC.env$outputdir)

  # Use define model options (created on model_compute) and compute the model
  # uses biomod2 model options
  check_computed <- capture.output(
    model_sdm <-
      biomod2::BIOMOD_Modeling(
        bm.format = model_compute,
        models = model_algorithm,
        bm.options = model_options,
        nb.rep = model_options_algorithm$NbRunEval,
        data.split.perc = model_options_algorithm$DataSplit,
        weights = model_options_algorithm$Yweights,
        prevalence = model_options_algorithm$Prevalence,
        var.import = model_options_algorithm$VarImport,
        metric.eval = model_options_algorithm$biomod_eval_method,
        save.output = TRUE,
        scale.models = model_options_algorithm$rescale_all_models,
        do.full.models = model_options_algorithm$do_full_models,
        modeling.id = model_options_algorithm$model_id
      )
  )

  model_sdm <- EC_check_computed(
    model_sdm, check_computed, model_options_algorithm
  )
  # save out the model object
  EC_save(model_sdm, name = paste0(
    model_options_algorithm$species_algo_str,
    "_model_object.rds"
  ), EC.env$outputdir)

  # Generate and save a variable importance plot (VIP) and the model object
  x.data <- attr(model_compute, "data.env.var")
  y.data <- attr(model_compute, "data.species")
  data1 <- data.frame(y.data, x.data)

  tryCatch(
    {
      EC_plot_VIP_glm(EC.env,
        fittedmodel = model_sdm,
        data1 = data1,
        pdf = TRUE,
        filename = paste("VIP_plot",
          model_options_algorithm$species_algo_str,
          sep = "_"
        ),
        this.dir = paste(response$occur_species,
          "/models/",
          EC.params$modeling_id,
          sep = ""
        )
      )
    },
    error = function(e) {
      message("VIP plot failed:\n", e)
    }
  )

  # 1. Projection without constraint if all env data layers are continuous
  model_options_algorithm$selected_models <-
    biomod2::get_built_models(model_sdm, run = "allRun")
  if (constraint$genUnconstraintMap &&
    all(predictor$type == "continuous")) {
    tryCatch(
      {
        project_model_current_unconstrained(
          EC.env,
          constraint,
          predictor,
          model_sdm,
          dataset,
          model_options_algorithm,
          model_algorithm,
          EC.params
        )
      },
      error = function(e) {
        message("Project unconstrained failed:\n", e)
      }
    )
  }
  # dataset <- EC_check_new_levels(model_sdm, dataset)

  project_model_current(
    EC.env,
    model_sdm,
    dataset,
    model_options_algorithm,
    model_algorithm,
    EC.params
  )

  # 3. Save evaluation statistics and graphs
  EC_save_BIOMOD_model_eval(
    EC.env,
    model_sdm,
    model_options_algorithm$species_algo_str,
    model_algorithm
  )

  # Remove temp raster files
  terra::tmpFiles(remove = TRUE)
  raster::removeTmpFiles()

  message("Completed species distribution modelling with algorithm 'GLM'\n")
}

#' Set parameters for running the GLM algorithm
#'
#' @param a a list with the following elements:
#'   \describe{
#'     \item{type}{(character) The type of GLM model to run. Possible values are
#'     "simple", "quadratic", or "polynomial".}
#'     \item{interaction.level}{(integer) The level of interaction terms to
#'     include in the model.}
#'     \item{test}{(character) The type of model selection criterion to use.
#'     Possible values are "AIC", "BIC", or "none".}
#'     \item{family}{(character) The family of the GLM model to use. Possible
#'     values are "binomial", "gaussian", "gamma", or "inverse.gaussian".}
#'     \item{mustart}{(numeric) Starting values for the vector of means.}
#'     \item{control}{(list) A list of control parameters for the GLM algorithm.
#'     The following elements can be included:
#'       \describe{
#'         \item{epsilon}{(numeric) A positive convergence tolerance.}
#'         \item{maxit}{(integer) The maximal number of IWLS iterations.}
#'         \item{trace}{(logical) A logical indicating if output should be
#'         produced for each iteration.}
#'       }
#'     }
#'   }
#'
#' @return A list with the specific parameters to run the GLM algorithm.
EC_options_glm <- function(a) {
  list(
    type = a$type,
    interaction.level = a$interaction_level,
    myFormula = NULL,
    test = a$test,
    family = a$family,
    mustart = a$mustart,
    control = list(
      epsilon = a$control_epsilon,
      maxit   = a$control_maxit,
      trace   = a$control_trace
    )
  )
}

#' Plotting Variable Importance in Generalized Linear Models (GLMs)
#'
#' This function generates a Variable Importance Plot (VIP) for a fitted GLM
#' model.
#'
#' @param EC.env
#' @param fittedmodel An object obtained from biomod2 "BIOMOD_Modelling"
#' @param cor.method A character string with the correlation method to be used
#' for the VIP. Can be either "pearson" for linear data or "spearman" for
#' non-linear
#' @param pdf A logical to indicate whether to output the plot as a pdf file or
#' not
#' @param biom_vi A logical to indicate whether to use a function/algorithm
#' other than biomod2 "variables_importance"
#' @param output.table A logical to indicate whether to output a csv file with
#' GLM parameters and the 95% confidence interval
#' @param data1 A data frame with response and predictors variables; should
#' contain all predict variables
#' @param this.dir A route to access biomod2 model (not the model name)
#' @param filename A character string with the filename to be saved, without the
#' file extension
#'
#' @return A Variable Importance Plot (VIP) for a fitted GLM model
EC_plot_VIP_glm <- function(EC.env,
                            fittedmodel,
                            cor.method = c("pearson", "spearman"),
                            pdf = TRUE,
                            biom_vi = FALSE,
                            output.table = FALSE,
                            data1,
                            this.dir,
                            filename) {
  # Transfom NAs to '0'
  data1$y.data[is.na(data1$y.data)] <- 0

  # Extract the root of directory used by biomod2 to save model results
  directory <- dir(this.dir)

  # Select the full model generated
  filekeep <- paste0(this.dir, "/", grep("GLM", directory, value = TRUE))
  filekeep2 <- grep("allRun", filekeep, value = TRUE)
  working <- load(filekeep2)
  fittedmodel <- biomod2::get_formal_model(eval(parse(text = working)))

  # First, save a summary text file with output
  dir.create(file.path2(EC.env$outputdir, "eval_tables/"),
    showWarnings = FALSE
  )
  sink(paste0(EC.env$outputdir, "/eval_tables/Summary_GLM.txt"),
    type = "output"
  )
  print(summary(fittedmodel$model))
  sink()

  # Then, save standard diagnostic plots
  dir.create(file.path2(EC.env$outputdir, "eval/"), showWarnings = FALSE)
  x <- gg_diagnostic_plot(fittedmodel)
  EC_save_png(x[[1]], x[[2]], x[[3]], x[[4]],
    ncol = 2,
    nrow = 2,
    filename = "Diagnostic_plots_GLM",
    outputdir = EC.env$outputdir
  )

  # Now, organise the data to create the VIP plot
  # These are the data used by biomod2 for model fitting
  fitteddata <- fittedmodel$data
  nd <- dim(fitteddata)[2]
  sub.data <- fitteddata[, 2:nd, drop = FALSE]

  # Can only scale numeric data
  cat.data <- Filter(is.factor, sub.data)
  sub.data <- Filter(is.numeric, sub.data)
  RespV <- fitteddata[, 1, drop = FALSE]
  rescaled.data <- scale(sub.data)

  # Attributes(rescaled.data)
  all.data <- cbind(RespV, as.data.frame(rescaled.data), cat.data)

  rescaled.glm <- glm(fittedmodel$formula, data = all.data, family = binomial)

  scaled <- as.data.frame(cbind(
    coef(rescaled.glm),
    confint(rescaled.glm)
  ))[-1, ]
  raw <- as.data.frame(cbind(
    coef(fittedmodel),
    confint(fittedmodel)
  ))[-1, ]

  # Variable importance plot in terms of relative effect size
  # The relative effect size is defined as the coefficient estimated based on
  # scaled data
  nx <- length(coef(fittedmodel)[-1])
  df1 <- as.data.frame(cbind(1:nx, round(scaled, 3)))

  names(df1) <- c("xvar", "meanest", "lower", "upper")
  df1$xvar <- factor(df1$xvar, labels = rownames(df1))

  ps <- gg_errorbars(df1)

  # Table version of above plot
  df2 <- as.data.frame(cbind(1:nx, round(raw, 3)))
  names(df2) <- c("xvar", "meanest", "lower", "upper")
  df2$xvar <- factor(df2$xvar, labels = rownames(df2))

  if (output.table) {
    df1t <- df1
    df2t <- df2
    names(df1t) <- c("x.var", "coeff.est.scaled", "lower", "upper")
    names(df2t) <- c("x.var", "coeff.est.raw", "lower", "upper")
    dfout <- cbind(df2t, df1t)
    write.csv(dfout,
      file = paste(filekeep, "paraest_out.csv", sep = "_"),
      row.names = FALSE
    )
  }

  # The heatmap in terms of correlation among numerical predictor variables
  rescdata <- Filter(is.numeric, rescaled.glm$data[, -1, drop = FALSE])
  if (ncol(rescdata) > 0) {
    xx <- cor(rescdata, method = cor.method[[1]])
    lower_tri <- xx
    lower_tri[upper.tri(lower_tri)] <- NA
    corr_df <- reshape2::melt(lower_tri, na.rm = TRUE)

    # Save as variable correlation plot.
    pheat <- gg_heatmap(corr_df)
    filename1 <- sub("vip_plot", "variable_correlations", filename)
    EC_save_png(pheat,
      ncol      = 1,
      nrow      = 1,
      filename  = paste0(filename1),
      outputdir = EC.env$outputdir
    )
  }

  if (biom_vi) {
    vi_biomod <- biomod2::bm_VariablesImportance(
      bm.model = fittedmodel,
      expl.var = data1[, -1]
    )
    df_vi <- data.frame(
      variable = colnames(data1)[-1],
      importance = vi_biomod[, 1]
    )
  } else {
    # variable importance plot in terms of AIC scores which represent the
    # information loss. E.g., the AIC score of a predictor variable representing
    # the information loss if this variable is not included in the selected
    # model.
    response_df <- data1[, 1] # nolint: object_usage_linter.
    predictor_df <- data1[, -1, drop = FALSE]
    glm.all <- glm(
      formula = response_df ~ .,
      family = binomial,
      data = predictor_df
    )

    # Calculate delta AIC
    Xaic <- vector("numeric", length = ncol(predictor_df))
    for (i in seq_len(ncol(predictor_df))) {
      subdf <- predictor_df[, -i, drop = FALSE]
      glm.one <- glm(
        formula = response_df ~ .,
        family = binomial,
        data = subdf
      )
      Xaic[i] <- AIC(glm.one)
    }

    # Convert to data.frame
    df_vi <- data.frame(
      variable = colnames(predictor_df),
      importance = Xaic - AIC(glm.all)
    )
  }

  df_vi <- df_vi[order(df_vi$importance, decreasing = FALSE), ]
  df_vi$variable <- factor(
    seq_len(nrow(df_vi)),
    levels = seq_len(nrow(df_vi)),
    labels = df_vi$variable
  )

  ppv <- gg_variable_importance(df_vi,
    type = if (biom_vi) {
      "biomod"
    } else {
      "aic"
    }
  )

  # Save as variable relative contribution plot.
  filename1 <- sub("vip_plot", "variable_relative_contribution", filename)

  EC_save_png(ps, ppv,
    ncol = 2,
    nrow = 1,
    filename = filename1,
    outputdir = EC.env$outputdir
  )
}
