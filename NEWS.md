# ecocommons 1.2.13

- fix the evaluation statistical table of maxent
- change the width of response curve plots of glm


# ecocommons 1.2.12

 - improve the visualisation of the response curve plots of glm

# ecocommons 1.2.11

## Improvements

-   Constraint masking of climate layers using alternate approach (via windowing). Improves performance
-   Avoid applying constraint to climate data when constraint method is environmental envelope 

# ecocommons 1.2.10

## Fixes

-   Entend "base layer" changes below to climate change experiments

# ecocommons 1.2.9

## Failed test fixes

-   Ensure dashes (-) are substituted for underscores (\_) in predictor layers
    -   data.frame(x) converts '-' to '.' causing name mismatch errors

## Improvements

-   Option for users to select a "base layer" for experiments which determines:
    -   The resolution for resampling spatial rasters;
    -   The CRS for reprojecting the raster stack, occurence/absence points and training/prediction constraints
-   Additional handling allowing models to be trained under a different constrained region to which predictions are made.

# ecocommons 1.2.8

## Failed test fixes

-   Ensure all study area constraint longitudes are from -180 to 180 degrees
    -   Fixes failed test 00041 all algorithms
-   Add implementation for Maxent bias layer
    -   Fixes for failed test 00042 maxent

## Improvements

-   Alternative CRS for occurrences
    -   Allow non-WGS84 coordinates for occurrences via EPSG codes

# ecocommons 1.2.7

## Failed test fixes

-   Ensure/force GeoTiffs within ensemble experiments have matching extents
    -   Fixes failed test 00028 ensemble job
-   Removal of missing occurrence records
    -   Fixes failed test 00031 maxent job
-   Climate change experiments using terra (as per SDMs)
    -   Fixes failed test 00035 cc (maxent) job
    -   Fixes failed test 00040 cc (bioclim) job
-   Added MaxEnt replicate capability to climate change experiments
    -   Fixes failed test 00003 cc (maxent with replicates) job
    -   Avoid unnecessary warnings

## Climate change experiment improvements

-   Corrected datatype in categorical change GeoTiff
-   Avoid empty trim when all cells less than threshold when calculating centre gravity metric

# ecocommons 1.2.6

## Large constraint fix

-   Re-implemented the filtering of occurrence points to within the area constraint using terra in EC_SDM_geoconstrained.R
    -   Deals with large global and complex constraint areas that were causing errors
    -   Better captures the occurrence points within the constraint area. A side effect of this, is that some models sensitive to randomness (via seed) may fail to rerun/calibrate successfully

## Climate change experiment extent fix

-   Ensure/force GeoTiffs within climate change experiments have matching extents with those from the original experiments

# ecocommons 1.2.3

## Tiling and parallel workflows

-   Adds a new function `par_pred()` to tile and predict in parallel
    -   Called within `project_model_current_unconstrained()`, if the number of cells in the raster is greater than 250000000 (250 million)
    -   Limited to large raster layers only for further testing and monitoring in the testing and production environments of the platform
    -   This is a potential fix for failures during platform pipelines when processing / predicting large raster layers, such as the global datasets (Worldclim, Chelsa)

## Minor improvements and bug fixes

-   Adds a `pkgdown` documentation website at <https://ecocommons-australia.gitlab.io/ecocommons-platform/ecocommons/>
-   First Vignette (title: `ecocommons`) added to the package
-   Add example data to `inst/` folder, using in `ecocommons` Vignette
-   Updates to `EC_check_computed()` with new error message to inform a user when all models fail, and the chosen data split percentage was 100%
    -   `EC_modelling_ann()` is particularly prone to this error, where using a 100% split can fail but models built using 80% for example would have worked.
-   Fix bug in `EC_projecting_cc()`
-   Fix bug in `EC_projecting_cc()` with `SRE` not working because of predictors in different order to original model
-   Enable clamping mask for `biomod` / dist model future projection fixing bug where prediction would previously be NaN in some cases
-   Adding developer functions in `zzz.R` for debugging platform jobs quickly
    -   Although not expected to be used by users, these functions have potential future utility as a way to quickly set up and run experiments locally, as well as archive them.

## Local use improvements

-   Adding more user friendly functions for local use, e.g. `EC_reset_env()`, `EC_get_data()`, `EC_build_data()`, `EC_explore_built_data()`. These functions are showcased in the `ecocommons` Vignette.

# ecocommons 1.2.2 (2023-03-28)

## Major changes

-   `EC_modelling_maxent()` overhaul in progress:
    -   Will now handle and evaluate replicates when repeats or cross-folds \> 1
    -   Now uses maximum sensitivity and specificity threshold (cloglog) by default to generate binary map (habitat suitability)
    -   Predictions with replicates will output GeoTIFFs containing one layer per model, and a mean layer (pending pipeline changes TBD)
    -   Removed output of MaxEnt model objects for cloglog and logistic as these were identical to the raw object - transformation occurs for prediction
    -   Future improvements pending imrpvoed front end inputs to support more complicated workflows and user chocie for which models and thresholds to use for evaluations, or how results for multiple predictions should be summarised and reported

## Minor improvements and bug fixes

-   Data split improving handling for `EC_modelling_*()` family
    -   Improving and adding handling for models when using data split and repeats
        -   For now this applies only to `biomod2` models, and is a work in progress
        -   Model evaluation tables saved in `eval_tables` (e.g. Loss-function, Evluation-statistics, etc.) will now show values for each individual model that was generated, instead of just evaluating the "full run" model
        -   Two new plots in `eval` including a mean and standard deviation plot and a boxplot of the evaluation statistics for comparison of all models that were generated
-   Enable `EC_modelling_CTA()` to run with categorical predictors
-   Added `tryCatch()` for unconstrained projections
-   Fix bug in `EC_modelling_bioclim()` causing projection to fail
-   Fix bug `EC_projecting_ensemble()`
-   `EC_save_projection_generic()` rolled out for most model workflows now, replacing base R raster plots with `ggplot` versions.

## 1.2.1 - 2023-03-20

## Minor improvements and bug fixes

-   Fix bug in `EC_raster_resampled()` during dummy raster creation affecting re-sampling process
-   Fix bug in `EC_SDM_geoconstrained()` which was causing NaNs in values over 255 due to `terra` assuming 8 bit integer format for small files
-   Add `EC_save_projection_generic()` which will replace `EC_save_projection()` and can be used to plot predictions with `ggplot2` and `tidyterra` for both `SpatRaster` and `RasterLayer` formatted predictions.
    -   Rolling out starting with `EC_modelling_bioclim()`.
