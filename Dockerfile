FROM registry.gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/ec-base-r4.2:1.2.0

ARG ECR_PACKAGE_REF=main

# IGNORE: For local building on mac M1 ----------------
# FROM --platform=linux/amd64 registry.gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/ec-base-r4.2:1.1.0

# Need these for unittest
# Disabling - testthat is required by devtools and installed on base image
# here is installed on base image

# RUN R -vanilla -e 'install.packages("testthat", dep=T)'
# RUN R -vanilla -e 'install.packages("here", dep=T)'

# For local container use, you can disable this line, and mount the repo locally
# RUN R -vanilla -e 'devtools::install_git("https://gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons.git", ref="${ECR_PACKAGE_REF}")'
RUN R -vanilla -e 'devtools::install_git("https://gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons.git", ref="${ECR_PACKAGE_REF}", build_manual = FALSE, build_vignettes = FALSE, upgrade = "never")'

# When package changes are pushed to main branch this image can be rebuilt
# If changes affect dependencies or package versions, the renv.lock should be
# updated, and the base image rebuilt. 

# In the future, for quick patches, it should also be possible to call renv.lock
# here, check the differences between the cached packages and the packages in
# the updated lock file, and update the packages needed? To save rebuilding the
# base image? 