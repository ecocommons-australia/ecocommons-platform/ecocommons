# Renv package management

1. EC-R package uses Renv, which is an R package for dependency management
2. A renv project has a `lockfile` that contains a list of all packages used in
   the project, and their versions.  
3. The lockfile can help ensure that all team members are using the same
   versions of packages.
4. Besides this, it is used to install all required R packages when rebuilding the base image
   (`ecocommons/ec-base-r4.2`), and therefore should not be modified unless
   necessary.

## Local development of ecocommons R package

1. Clone the ecocommons R package gitlab repository
2. Create a branch, referencing the jira ticket number you will be working on
3. Open the `.Rprofile` file in the root directory of the project
4. Remove the comment so that it reads:
   1. `source("renv/activate.R")` instead of `# source("renv/activate.R")`
   2. For working on a branch it is ok to leave this uncommented. This command
      loads and runs the renv setup commands when you open the R project. But
      before merging to main this should be commented out again.
5. Open `ecocommons.Rproj` in RStudio
6. Renv should be detected and automatically bootstrap
7. When it finishes, it should ask you to run `renv::restore()`, which will
   install all required packages, based on the lockfile
8. Done!

## Changes to packages (new packages or updating current packages)

1. If a new package is needed for the project, install the package, and then run
   `renv::snapshot()` to update the lockfile
   1. Package installations should be discussed between team members^1, so that
      the base image can be rebuilt and tested, using the updated lock file.
   2. ^1 Potentially including biosecurity commmons as well - there is a potential for
         inter-dependencies and conflicts arising. this is yet to be discussed,
         but will be soon. 
2. Avoid updating packages in general if possible. We might work out a plan to scope
   necessary updates at a defined interval. Sometimes updates are necessary
   but they may also introduce changes that can effect the workflow (and
   sometimes these changes are not fully documented leading to unexpected
   surprises *cough* biomod *cough*), so full testing is required after changes. 
   1. In general this should happen on a branch. 
   2. Particularly if a large amount of package updates is required, or updates to one of the key
      dependencies (such as terra, biomod2, ggplot2 etc.), this should be implemented on a
      branch first, to test with both local `testthat` tests, as well as the
      full `SDM-function` pipeline, before merging to main 
   3. Documentation for how to force installation of an ecocommons r package
      development branch, for use on the local pipeline, is in progress
3. For local use, if you would like to install or load miscellaneous local
   packages that aren't needed for the project, there is no need to use
   renv::snapshot().
