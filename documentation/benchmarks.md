## Benchmarks

Benchmarks (elapsed time) now appear in the `.Rout` file for experiments. These
represent time in seconds or minutes for different stages in the R process. It
will not reflect the total job time (preparing inputs, outputs etc), just the
time spent in the R functions. They can be useful for comparing different runs
or finding bottlenecks.

## 1.2.2 - 29-03-2022

These are a few examples for some bigger jobs and a small job for comparison.
The time taken in `build dataset` is mostly related to re-projection and
resampling of input layers. The time spent in the `model` stage includes
training the model (usually fast) and predictions (which can be slow).

The TL;DR of this small sample size is:

- Resampling and re-projecting is the bottleneck
- Re-projecting a a single 50m layer to WGS84 took **~65 minutes**.
- Re-sampling 3 layers to 90 meter resolution took **32 minutes**.
- Running a job which needs both re-projection and resampling would increase the
  total time exponentially.  
- MaxEnt jobs run ~5 to 10 times slower than other algorithms.

---

- Server: Prod
- Title: medium-butterfly
- Occurrence:
  - 387 presence points (*L. jungguy*)
- Predictors:
  - 1 x 90m NVIS
  - 2 x 1km current climate
  - scale to finest = TRUE (scale to 90m)
- Algos:
  - GAM // passed
    - Build dataset: 32 minutes
    - Model: 1.2 minutes
  - ANN // passed
    - Build dataset: 32 minutes
    - Model: 40 seconds
      - 30/40s on projecting constrained
  - MaxEnt // passed
    - Build dataset: 39 minutes
    - Model: 10.5 minutes
  - BRT // passed
    - Build dataset: 32 minutes
    - Model: 12 seconds

- Server: Prod
- Title: unorthod-ox
- Occurrence:
  - 387 presence points (*L. jungguy*)
- Predictors:
  - 1 x 50m Catchment Scale Land Use of Australia, ABARES, Secondary Categories
- Algos:
  - FDA // failed
    - Build dataset: 65.72 minutes
    - Failed when trying to drop levels from the raster for prediction (R bug -
      possibly due to cells that are underrepresented and a filter needs to be
      added)
- MaxEnt // passed
  - Build dataset: 65.58 minutes
    - Re-projecting raster from (EPSG:) 3577 to epsg:4326
  - Model: 9.99 minutes

- Server: Prod
- Title: slow eel
- Occurrence:
  - ~45 000 presence points (tree creeper)
- Predictors:
  - 2 x 1km Aus current climate
- Algos:
  - ANN GLM GAM BRT etc // passed
    - all < ~3.5 minutes
  - MaxEnt // passed
    - Build dataset: 15 seconds
    - Model: 19.12 minutes
